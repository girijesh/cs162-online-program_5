#include "superhero.h"

/*
  Girijesh Thodupunuri
  CS162
  Program 3

*/
// This file contains all the main operations for the program 3 regarding superheros.

int main()
{
    Node *head = NULL;

    int choice; // User's choice to add another superhero
    do
    {
        Superhero new_hero; // Create a new superhero
        create_superhero(new_hero);
        cout << new_hero.name << endl;
        cout << "Where would you like to insert this hero in your list? (1) Beginning or (2) End? ";
        cin >> choice;
        cin.ignore();

        if (choice == 1)
        {
            insertAtBeginning(head, new_hero);
        }
        else if (choice == 2)
        {
            appendAtEnd(head, new_hero);
        }

        cout << "Add another superhero? (1) Yes (0) No: ";
        cin >> choice;
        cin.ignore();
    } while (choice != 0);

    cout << "\n Here are all the Superheroes you have added:\n";
    displaySuperheros(head);

    cout << "Deallocating the list...\n";
    deallocateList(head);

    return 0;
}
