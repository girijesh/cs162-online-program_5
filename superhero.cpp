#include <iostream>
#include <string>
#include "superhero.h"
using namespace std;

/*
  Girijesh Thodupunuri
  CS162
  Program 5

*/
// This file contains all the function definitions for program 5 regarding superheroes

// Function to create a new superhero
// Inputs the superhero details from the user and stores them in the Superhero struct
void create_superhero(Superhero &a_hero)
{
    cout << "Enter the name of the superhero: ";
    cin.get(a_hero.name, NAME, '\n');
    cin.ignore(100, '\n');

    // Temporary array to store the backstory
    char temp[500];
    cout << "Please enter a backstory: ";
    cin.get(temp, 500, '\n');
    cin.ignore(100, '\n');

    // Dynamically allocate memory for the backstory and copy the contents
    a_hero.backstory = new char[strlen(temp) + 1];
    strcpy(a_hero.backstory, temp);

    cout << "Please enter the theme of the superhero: ";
    cin.get(a_hero.theme, THEME, '\n');
    cin.ignore(100, '\n');

    cout << "Please enter the powers of the superhero: ";
    cin.get(a_hero.powers, POWERS, '\n');
    cin.ignore(100, '\n');

    cout << "Please enter the love interest of the superhero: ";
    cin.get(a_hero.love_interest, LOVE, '\n');
    cin.ignore(100, '\n');
}

// Function to insert a new superhero at the beginning of the linked list
void insertAtBeginning(Node *&head, Superhero superhero)
{
    // Create a new node
    Node *new_node = new Node;
    new_node->data = superhero;
    new_node->next = head;

    // Update the head to point to the new node
    head = new_node;
}

// Function to append a new superhero at the end of the linked list
void appendAtEnd(Node *&head, Superhero superhero)
{
    // Create a new node
    Node *new_node = new Node;
    new_node->data = superhero;
    new_node->next = NULL;

    if (head == NULL)
    {
        // If the list is empty, set the new node as the head
        head = new_node;
    }
    else
    {
        // Traverse to the last node
        Node *curr = head;
        while (curr->next != NULL)
        {
            curr = curr->next;
        }
        // Append the new node at the end
        curr->next = new_node;
    }
}

// Function to display all the superheroes in the linked list
void displaySuperheros(Node *head)
{
    Node *curr = head; // pointer to traverse the list
    while (curr != NULL)
    {
        cout << "Name: " << curr->data.name << endl;
        cout << "Backstory: " << curr->data.backstory << endl;
        cout << "Theme: " << curr->data.theme << endl;
        cout << "Powers: " << curr->data.powers << endl;
        cout << "Love Interest: " << curr->data.love_interest << endl;
        cout << endl;
        curr = curr->next;
    }
}

// Function to deallocate the memory used by the linked list
void deallocateList(Node *head)
{
    Node *curr = head; // pointer to traverse the list
    while (curr != NULL)
    {
        Node *next = curr->next;
        deallocateNode(curr);
        curr = next;
    }
    head = NULL;
}

// Function to deallocate the memory used by a single node
void deallocateNode(Node *node)
{
    delete[] node->data.backstory;
    delete node;
}

