#include <iostream>
#include <cctype>
#include <cstring>
using namespace std;


/*
  Girijesh Thodupunuri
  CS162
  Program 5

*/
// This file contains all constant variables, struct and prototypes for program 5 regarding superheroes

const int NAME = 31;    // Max length of the superhero name   
const int LOVE = 31;    // Max length of the love interest
const int THEME = 50;   // Max length of the theme  
const int DESCRIPTION = 131; // Max length of the backstory
const int POWERS = 100; // Max length of the powers
struct Superhero {
    char name[NAME]; // Name of the superhero
    char* backstory; // Backstory of the superhero
    char theme[THEME];// Theme of the superhero
    char powers[POWERS]; // Powers of the superhero
    char love_interest[LOVE]; // Love interest of the superhero
};

struct Node {
    Superhero data; // Data of the node
    Node* next; // Pointer to the next node
};

void create_superhero ( Superhero & a_hero);
void insertAtBeginning(Node*& head, Superhero superhero);
void appendAtEnd(Node*& head, Superhero superhero);
void displaySuperheros(Node* head);
void deallocateList (Node * head);
void deallocateNode (Node * node);

